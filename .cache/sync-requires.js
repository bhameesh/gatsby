const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => m && m.default || m


exports.components = {
  "component---cache-dev-404-page-js": hot(preferDefault(require("/home/cuckoo/gitGatsby/.cache/dev-404-page.js"))),
  "component---src-pages-contact-js": hot(preferDefault(require("/home/cuckoo/gitGatsby/src/pages/contact.js"))),
  "component---src-pages-form-js": hot(preferDefault(require("/home/cuckoo/gitGatsby/src/pages/form.js"))),
  "component---src-pages-index-js": hot(preferDefault(require("/home/cuckoo/gitGatsby/src/pages/index.js")))
}

